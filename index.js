
var express = require('express');

var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookieParser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

app.set('port', process.env.PORT || 3000);

app.get('/', function(req, res) {
	res.send('so ugly');
});

app.listen(app.get('port'), function(){
	console.log('Express started press Ctrl-C to terminate');
});